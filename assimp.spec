Name:          assimp
Version:       5.3.1
Release:       1
Summary:       Library to load and process various 3D model formats into applications.
License:       BSD and MIT and LGPL-2.1 and LGPL-2.0 and GPL-2.0 and LGPL-3.0 and GPL-3.0
URL:           http://www.assimp.org/
#wget https://github.com/assimp/assimp/archive/v%{version}.tar.gz
#tar xf v%{version}.tar.gz
#cd assimp-%{version}
#rm -rf test/models-nonbsd
#cd ..
#tar czf assimp-%{version}-free.tar.xz assimp-%{version}
Source0:       assimp-%{version}-free.tar.xz
BuildRequires: gcc-c++ boost-devel cmake dos2unix irrlicht-devel irrXML-devel
BuildRequires: doxygen poly2tri-devel gtest-devel pkgconfig(zziplib)
BuildRequires: pkgconfig(zlib) pkgconfig(minizip) gmock-devel make
BuildRequires: pkgconfig(python3) python3-rpm-macros
Provides:      bundled(polyclipping) = 4.8.8 bundled(openddl-parser)

%description
Assimp is a library to load and process geometric scenes from various data formats.
Assimp aims to provide a full asset conversion pipeline for use in game
engines and real-time rendering systems of any kind, but is not limited
to this purpose.

%package       devel
Summary:       Headers and libraries for assimp
Requires:      assimp = %{version}-%{release}

%description devel
This package provides the header files and libraries
for assimp. Developers use it to develop programs.

%package -n    python3-assimp
Summary:       Python3 bindings for assimp
BuildArch:     noarch
Requires:      assimp = %{version}-%{release} python3
Provides:      assimp-python3 = %{version}-%{release}
Obsoletes:     assimp-python3 < 3.1.1

%description -n python3-assimp
This package provides the PyAssimp3 python bindings

%package       help
Summary:       Assimp help doc
BuildArch:     noarch
Provides:      assimp-doc = %{version}-%{release}
Obsoletes:     assimp-doc < %{version}-%{release}

%description   help
Assimp help doc.

%prep
%autosetup -n assimp-%{version} -p1

%build
%cmake -DASSIMP_BUILD_ZLIB=OFF 
       
%make_build

%install
%make_install
install -d %{buildroot}%{python3_sitelib}/pyassimp/
install -m 0644 port/PyAssimp/pyassimp/*.py %{buildroot}%{python3_sitelib}/pyassimp/

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc CREDITS LICENSE
%{_libdir}/*.so.*

%files devel
%{_includedir}/assimp
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/cmake/*

%files help
%{_docdir}/*

%files -n python3-assimp
%doc port/PyAssimp/README.md
%{python3_sitelib}/pyassimp

%changelog
* Wed Oct 18 2023 chenyaqiang <chengyaqiang@huawei.com> - 5.3.1-1
- update to 5.3.1

* Sat Feb 04 2023 wenchaofan <349464272@qq.com> - 5.2.5-1
- Update to 5.2.5 version

* Wed Jun 15 2022 wulei <wulei80@h-partners.com> - 5.2.4-1
- Upgrade to 5.2.4

* Mon Jan 4 2021 Ge Wang <wangge20@huawei.com> - 3.3.1-22
- Modify homepage url and license infomation

* Fri Dec 25 2020 wangxiao <wangxiao65@huawei.com> - 3.3.1-21
- Remove unnessary BuildRequire DevIL

* Wed Oct 21 2020 chengzihan <chengzihan2@huawei.com> - 3.3.1-20
- Remove Subpackage python2-assimp

* Wed Sep 9 2020 Ge Wang <wangge20@huawei.com> - 3.3.1-19
- Modify Source0 Url

* Fri Feb 14 2020 likexin <likexin4@huawei.com> - 3.3.1-18
- Package init
